FROM nginx:1.18.0-alpine

RUN apk add --no-cache certbot curl

RUN mkdir /etc/letsencrypt

COPY update-secret.sh /
COPY request-cert.sh /
