kube-letsencrypt
================

Helper to automate Let's Encrypt certificate requests in Kubernates.

There's are lots of repos like this online, but this one is mine. It exists so
that I don't have to worry about supply chain attacks.
