#!/bin/sh

set -e

CERTPATH=/etc/letsencrypt/live/abstractbinary.org

function upload_secret() {
    sed -e "s/TLSCERT/$(cat $CERTPATH/fullchain.pem | base64 | tr -d '\n')/" \
        -e "s/TLSKEY/$(cat $CERTPATH/privkey.pem | base64 | tr -d '\n')/" \
        | curl \
              --cacert /var/run/secrets/kubernetes.io/serviceaccount/ca.crt \
              -H "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
              -XPATCH \
              -H "Accept: application/json, */*" \
              -H "Content-Type: application/strategic-merge-patch+json" \
              -d @- \
              https://kubernetes/api/v1/namespaces/default/secrets/letsencrypt-certs \
              -k -v
}

cat <<EOF | upload_secret
{
    "kind": "Secret",
    "apiVersion": "v1",
    "metadata": {
        "name": "letsencrypt-certs",
        "namespace": "default"
    },
    "data": {
        "tls.crt": "TLSCERT",
        "tls.key": "TLSKEY"
    },
    "type": "Opaque"
}
EOF
