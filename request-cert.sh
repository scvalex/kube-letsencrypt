#!/bin/sh

set -e

certbot certonly --webroot -w /usr/share/nginx/html/ -n --agree-tos --email letsencrypt@abstractbinary.org --no-self-upgrade -d abstractbinary.org,scvalex.net --expand
